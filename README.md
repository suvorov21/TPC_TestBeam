# T2K TPC beam test analysis package

The package allow to convert data from MIDAS format (.mid) to ROOT format,
saving all the information about the ADC vs. time for each pad.
The included macros can be used for the data analysis.

For self-confident installation require:
* ROOT > v6.14.00  (v6.14.04 was tested and recommended)
* midas https://midas.triumf.ca/MidasWiki/index.php/Main_Page
* rootana https://midas.triumf.ca/MidasWiki/index.php/ROOTANA

*specify the correct paths in the setup.sh file!!!*

## COMPILE

To work with this package you need to compile the ROOT dictionary in order to be able to store c++ vectors
and vectors of vectors in the ROOT file.
To do this in the project folder run
```bash
rootcint -f  mydict.C -c LinkDef.h
```
This will generate mydict.C and mydict.h the name can be changed.

1. In the root folder the RootConverter should be compiled. Simply run
```bash
make clean
make
```
to build RootConverter.exe

2. To compile the macro you need run make command and specify
the macro code, the path to dictionary and the executable. In the macro folder:
```bash
$ make SRC="mydict.C your_macro.C" EXE="your_macro.exe"
```
This will produce executable your_macro.exe

## RUN

1. To run Root converter in the project folder run
```bash
RootConverter.exe mides_file.mid -ooutput_root_file.root
```
It will unpack the data in mides_file.mid into output_file.root

2. Before running the macro specify the path where you want to store the output histos in the
"prefix" variable. To run the macro call
```bash
./your_macro.exe -f output_root_file.root -o output_dir
```
This will produce all histos and output root files you requested in the macros code.

## DATA FORMAT

In order to be able to create your own macros here it is the data structure of the RootConverter
output file.


output_root_file.root:

    |-- TTree padData      # contains the pad data
        |-- PadphysChannels # vector<short>          # vector of phys.l channels for the pads
        |-- PadADCvsTime    # vector<vector<short> > # ADC vs. time
    |-- TTree femGeomTree  # contains geometry
        |-- iPad            # vector<int>            # i position of the physical channel
        |-- jPad            # vector<int>            # j position of the physical channel
        |-- xPad            # vector<double>         # x position of the physical channel
        |-- yPad            # vector<double>         # y position of the physical channel
        |-- dxPad           # vector<double>         # x size of a pad
        |-- dyPad           # vector<double>         # y size of a pad

So to look at the data:
```
loop over i in PadphysChannels[i] --> loop over hit pads
loop over j in PadADCvsTime[i][j] --> loop over ADC for pad i
```

Geometry:
```
Physical channels go from 0 to 1727
j corresponds to columns goes from 0 to 47
i corresponds to rows    goes from 0 to 35
x corresponds to colums
y corresponds to rows

                |
                | cosmic
                v
          47 ______
            |      |
beam        |      |
---->   y   |      |
            |      |
            |      |          __
          0 |______|         |__| 0.7 cm
            0      35       0.9 cm
                x
```

If the geometry was not included in the file, you can add it running the AddGeometryToOldFiles.exe
over file with data and very small file with proper geometry. It will save your time.

## SELECTION

macros/selection.C contains different selection that can be applied in the macro in order to fill
the histos only with selected events.
Selection will be compiled automatically with a macro.

```
SelectionTrackCosmic -> Selects cosmics tracks
SelectionTrackBeam   -> Selects beam tracks
SelectionIronSource  -> Selects isolated iron clusters
NewSelectionTrackCosmic -> embryonic version of 3D selection
```


## MACROS Summary

iron_calib.C:

	maximum adc in each event and each pad is stored and accumulated in PadDisplayADC histogram.
	number of hits per pad is stored in PadDisplayCnt histogram.

	same is done for to indep sets of data coming from the same run file:

	first 5k events,
	maxADC stored and accumulated in PadDisplayADC1
	number of hist per pad stored in PadDisplayCnt1

	all events but first 5k,
	maxADC stored and accumulated in PadDisplayADC2
	number of hist per pad stored in PadDisplayCnt2

	the idea is to divide PadDisplayADC/PadDisplayCnt to obtain average charge per hit in each pad. This is done for each of the 2 independent sets.

	to obtain something "normalized" the average charge per pad is divided by the average of the average charge per pad: (sum average charge per pad / number of pads)
	this normalized quantity is the intercalibration factor.

	the program divids the intercalibration factor from each one of the two data samples to check they are close to one, namely giving coherent results.

iron_ana.C:

	the program uses data from SelectionIronSource and stores the ADC content of each one of the clusters in ClusterCharge histogram. Them a double gaussian is fitted to obtain the parameters for 55Fe 6KeV main peak and a smaller ~3keV peak coming from argon scape peak. (At this moment gaussian fit is commented, the fit parameters often must be hardcoded depending on the file one wants to analyze).

display_sel.C:

	a simple macro that can be used to learn the workflow of the framework and the debbug and develop selections. It basically represent in canvas, event after event (it needs terminal inputs since "cin" is used to stop the loop at each iteration) the readout plain info (map of charge in pads) before (PadDisplayOriginal histogram), after (PadDisplayFinal histogram), accumulated charge summing adc event after event (PadDisplayDist), and also timemax distribution of the pads (padTimeDisplay).

line3Dfit.C:

	it is a modified copy and pasted piece of code that fits a 3D line to a TGraph2D. In principle it could be used in the future being called from selection.C NewSelectionTrackCosmic to develop a 3D selection. Is just a preliminary idea, the fit is still not properly fitting the 3D hits, however it shows a display that can be seen by using NewSelectionTrackCosmic in display_sel.C

prf_final.C:

	still to be explained...



Enjoy!!

