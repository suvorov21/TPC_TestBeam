#define THIS_NAME inter_calib
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "ilc/common_header.h"
#include "selection.C"

#define COSMIC
//#define PEAK2

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void inter_calib() {

#ifdef SEVERE_SUPPRESSION
  gErrorIgnoreLevel = kSysError;
#endif

  bool PLOT = true;

  vector<TString> listOfFiles;
  TString file="default.root";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file=gApplication->Argv(iarg);
      if (file.Contains(".root")) {

        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    }
  }

  int thr=0.;
  int TotalCharge = 0;
  int TotalCharge2 = 0;
//  int TotalHitPad=0., TotalHitPad1=0., TotalHitPad2=0.;
  float TotalChargeHitPad=0., TotalChargeHitPad1=0., TotalChargeHitPad2=0.; 
  TString prefix = "/home/TestBeam/OnlineTools/cjesus/output/";

    int itmax = -1;
  float ratio=0., incal=0., AvChargeHitAll=0., TotalChargeHitAll=0., q_avhitpad=0., q_avhit=0.; 
  float AvChargeHitAll0=0., AvChargeHitAll5=0., TotalChargeHitAll0=0., TotalChargeHitAll5=0.; 

  // define output
  TH2F* Total_qav=new TH2F("total_q","Before Inter Calibration (all events)",38,-1.,37.,50,-1.,49.);
  TH2F* NoApl_In_cal=new TH2F("BIC","Before Inter Calibration (events > 5000)",38,-1.,37.,50,-1.,49.);
  TH2F* Apl_In_cal=new TH2F("AIC","After Inter Calibration (events > 5000)",38,-1.,37.,50,-1.,49.);
  TH2F* In_cal = new TH2F("IC","Inter Calibration (ratio factor) (events <= 5000)",38,-1.,37.,50,-1.,49.);
  TH1F* r_n = new TH1F("r_n","ratio factor value; r; entries", 50, 0., 2.);

  TH2F* PadDisplayGeo=new TH2F("PadDisplayGeo","X vs Y of hits",50,-17.15,17.15, 38,-18.13,18.13);
  TH2F* PadDisplayH=new TH2F("PadDisplay1","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayH1=new TH2F("PadDisplay2","I vs J of hits",38,-1.,37.,50,-1.,49.);

  TH1F* ClusterCharge     = new TH1F("cluster_charge","Cluster charge",150,0,3000);
  TH1F* ClusterChargeCur  = new TH1F("cluster_chargeC","Cluster charge current",150,0,3000);

  TH2F* PadDisplayHIT     = new TH2F("PadDisplayH","I vs J of hits (all events)",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayHIT1     = new TH2F("PadDisplayH1","I vs J of hits (events <= 5000)",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayHIT2     = new TH2F("PadDisplayH2","I vs J of hits (events > 5000)",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayADC     = new TH2F("PadDisplayADC","I vs J of hits (all events)",36,0.,36.,48,0.,48.);
  TH2F* PadDisplayADC1     = new TH2F("PadDisplayADC1","I vs J of hits (events <= 5000)",36,0.,36.,48,0.,48.);
  TH2F* PadDisplayADC2     = new TH2F("PadDisplayADC2","I vs J of hits (events > 5000)",36,0.,36.,48,0.,48.);
  TH2F* PadDisplayCnt     = new TH2F("PadDisplayCnt","I vs J of hits (events <= 5000)",36,0.,36.,48,0.,48.);
  TH2F* PadDisplayCnt1     = new TH2F("PadDisplayCnt1","I vs J of hits (events <= 5000)",36,0.,36.,48,0.,48.);
  TH2F* PadDisplayCnt2     = new TH2F("PadDisplayCnt2","I vs J of hits (events > 5000)",36,0.,36.,48,0.,48.);
  
  TH1F* PadsPerCluster    = new TH1F("PadsPerCluster", "ppc", 8, 0., 8.);
  
  Int_t total_events = 0;
  Int_t total_NOiron_events =0.;
  Int_t sel_events = 0;

  vector< vector<int> > PadDisplay;
  vector< vector<int> > PadTime;
  vector< vector<int> > PadDisplayNew;
  vector< vector<int> > PadDisplayNew2;  
  vector< vector<int> > PadIntegral;
  vector< vector<vector<short> > > PadDisplayV;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  vector< vector<int > > EventClusters;

  TFile*f=0;
  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    if (f!=0) f->Close();
    cout << "opening file " << file <<  endl;
    f= new TFile(file);
    if (!f->IsOpen()) {
//      cout << " WARNING problem in opening file " << file << endl;
      return;
    }

    vector<int> *iPad(0);
    vector<int> *jPad(0);
    vector<double> *xPad(0);
    vector<double> *yPad(0);

    vector<double> *dxPad(0);
    vector<double> *dyPad(0);

    TTree * tgeom= (TTree*) f->Get("femGeomTree");
    tgeom->SetBranchAddress("jPad", &jPad );
    tgeom->SetBranchAddress("iPad", &iPad );

    tgeom->SetBranchAddress("xPad", &xPad );
    tgeom->SetBranchAddress("yPad", &yPad );

    tgeom->SetBranchAddress("dxPad", &dxPad );
    tgeom->SetBranchAddress("dyPad", &dyPad );
    tgeom->GetEntry(0); // put into memory geometry info
    cout << "Reading channel mapping" << endl;
    cout << "jPad->size()  " << jPad->size() <<endl;
    cout << "iPad->size()  " << iPad->size() <<endl;

    cout << "Reading geometry" << endl;
    cout << "xPad->size()  " << xPad->size() <<endl;
    cout << "yPad->size()  " << yPad->size() <<endl;
    cout << "dxPad->size() " << dxPad->size() <<endl;
    cout << "dyPad->size() " << dyPad->size() <<endl;

    int Imax = -1;
    int Imin = 10000000;
    int Jmax = -1;
    int Jmin = 10000000;

    for (unsigned long i = 0; i < jPad->size(); ++i) {
      if (Imax < (*iPad)[i])
        Imax = (*iPad)[i];
      if (Imin > (*iPad)[i])
        Imin = (*iPad)[i];
      if (Jmax < (*jPad)[i])
        Jmax = (*jPad)[i];
      if (Jmin > (*jPad)[i])
        Jmin = (*jPad)[i];
    }

    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    int Nevents=0;
    TTree *t = (TTree*) f->Get("padData");
    cout <<t->GetEntries() << " evts in file " << endl;

    vector<short>          *listOfChannels(0);
    vector<vector<short> > *listOfSamples(0);

    t->SetBranchAddress("PadphysChannels", &listOfChannels );
    t->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

    if (Nevents<=0) Nevents=t->GetEntries();
    if (Nevents>t->GetEntries()) Nevents=t->GetEntries();

    cout << "[          ] Nevents = "<<Nevents<<"\r[";

    //************************************************************
    //****************** LOOP OVER EVENTS ************************
    //************************************************************
    for (int ievt=0; ievt < Nevents ; ievt++){
      if (ievt%(Nevents/10)==0)
        cout <<"."<<flush;

      t->GetEntry(ievt);
      PadDisplayH->Reset();
      PadDisplayH1->Reset();

      // clean for the next event
      PadDisplayGeo->Reset();
      PadDisplayNew.clear();
      PadDisplayNew2.clear();
      PadDisplay.clear();
      PadIntegral.clear();
      PadTime.clear();

      PadDisplay.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplay[z].resize(Imax+1, 0);

      PadTime.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadTime[z].resize(Imax+1, 0);

      PadDisplayNew.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayNew[z].resize(Imax+1, 0);


      PadDisplayNew2.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayNew2[z].resize(Imax+1, 0);

      PadIntegral.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadIntegral[z].resize(Imax+1, 0);

      PadDisplayV.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayV[z].resize(Imax+1);

      ClusterChargeCur->Reset();


      //************************************************************
      //*****************LOOP OVER CHANNELS ************************
      //************************************************************
      for (uint ic=0; ic< listOfChannels->size(); ic++){
        int chan= (*listOfChannels)[ic];
        // find out the maximum
        float adcmax=-1;
        Int_t it_max = -1;

        // one maximum per channel
        for (uint it = 0; it < (*listOfSamples)[ic].size(); it++){
        //for (uint it = 10; it < 450; it++){
          int adc= (*listOfSamples)[ic][it];
          if (adc>adcmax && adc > thr) {
            adcmax = adc;
            it_max = it;
          }
        }

        int lower = max (it_max - 10, 5);
        int upper = min(it_max + 20, 490);
        int integral = 0;
        for (int it = lower; it < upper; it++){
          // stop integrating if noise
          if ((*listOfSamples)[ic][it] < 0) {
            if (it > it_max)
              break;
            else continue;
          }

          integral += (*listOfSamples)[ic][it];
        }

        // PadDisplayV[(*jPad)[chan]][(*iPad)[chan]] = (*listOfSamples)[ic];

        if (adcmax<0) continue; // remove noise
        //if ((*iPad)[chan] == Imin || (*iPad)[chan] == Imax ||
        //    (*jPad)[chan] == Jmin || (*jPad)[chan] == Jmax)
        //    continue;

        PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
        PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_max;
        PadIntegral[(*jPad)[chan]][(*iPad)[chan]] = integral;

      } //loop over channels
      ++total_events;

#ifndef SEVERE_SUPPRESSION
      Int_t gErrorIgnoreLevel_bu =0.;
      if (SUPPRESS_ERRORS) {
        gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
        gErrorIgnoreLevel = kSysError;
      }
#endif

if (SelectionIronSource(PadIntegral, PadTime, PadDisplayNew2)) continue;
    ++total_NOiron_events;

      // apply the selection
#ifdef COSMIC
      if (!SelectionTrackCosmic(PadDisplay, PadTime, PadDisplayNew))
          continue;
#else
      if (!SelectionTrackBeam(PadDisplay, PadTime, PadDisplayNew))
          continue;
#endif

#ifndef SEVERE_SUPPRESSION
      if (SUPPRESS_ERRORS)
        gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
#endif

      ++sel_events;


      // for (int j = 0; j < 48; ++j) {
      // for (int i = 0; i < 36; ++i) {
      // if (!PadDisplayNew[j][i]) PadIntegral[j][i] = 0;  
      // }
      // }

      // for (int j = 0; j < 48; ++j) {
      // int chargeTOT = 0;
      //   for (int i = 0; i < 36; ++i) {
      //     if (!PadDisplayNew[j][i])
      //       continue;
      //     PadDisplayNew[j][i]=PadIntegral[j][i];
      //   }
      // }

      // charge per cluster study
      vector<Float_t > cluster_charge;
      cluster_charge.clear();
      int offset = 0;

#ifdef COSMIC
      for (Int_t it_j = offset; it_j < Jmax+1; ++it_j) {
#else
      for (Int_t it_i = offset; it_i < Imax+1; ++it_i) {
#endif
        Int_t cluster = 0;
        Int_t pads_per_cluster = 0;
        int TotalHitPad=0., TotalHitPad1=0., TotalHitPad2=0.;
        float TotalChargeHitPad=0., TotalChargeHitPad1=0., TotalChargeHitPad2=0.;


#ifdef COSMIC
        for (Int_t it_i = 0; it_i < Imax+1; ++it_i) {
#else
        for (Int_t it_j = 0; it_j < Jmax+1; ++it_j) {
#endif
            cluster += PadDisplayNew[it_j][it_i];  
            if (PadDisplayNew[it_j][it_i] > 0.) {
              ++pads_per_cluster;
              PadDisplayHIT->Fill(it_i, it_j, 1);
              TotalHitPad++;
              TotalChargeHitPad += PadDisplayNew[it_j][it_i];
            }
            PadDisplayADC->Fill(it_i, it_j, PadDisplayNew[it_j][it_i]);
            PadDisplayCnt->Fill(it_i, it_j, 1);     

     //       if (sel_events <= 5000) {   
            if (sel_events <=5000) {    
              if(PadDisplayNew[it_j][it_i]>0){  
              PadDisplayADC1->Fill(it_i, it_j, PadDisplayNew[it_j][it_i]);
              PadDisplayCnt1->Fill(it_i, it_j, 1);
              TotalCharge+=PadDisplayNew[it_j][it_i];
              }
            }
            
            if (sel_events > 5000) {
              if(PadDisplayNew[it_j][it_i]>0){  
              PadDisplayADC2->Fill(it_i, it_j, PadDisplayNew[it_j][it_i]); 
              PadDisplayCnt2->Fill(it_i, it_j, 1);
              TotalCharge2+=PadDisplayNew[it_j][it_i];
              }            
            }
          }
          }

     Int_t TotalHitPad=0., TotalHitPad5=0., TotalHitPad0=0.;

        } // loop over events
      } // loop over files
  cout << endl;

int TotalEntries5k = PadDisplayADC1->GetEntries();
double ChargeAve = TotalCharge/TotalEntries5k;

#ifdef COSMIC
      for (Int_t it_j = 0; it_j < 48; ++it_j) {
#else
      for (Int_t it_i = 0; it_i < 36; ++it_i) {
#endif

#ifdef COSMIC
        for (Int_t it_i = 0; it_i < 36; ++it_i) {
#else
        for (Int_t it_j = 0; it_j < 48; ++it_j) {
#endif

  if(!PadDisplayADC1->GetBinContent(it_i+1,it_j+1)) continue;   
  PadDisplayADC1->SetBinContent(it_i+1,it_j+1,PadDisplayADC1->GetBinContent(it_i+1,it_j+1)/ChargeAve);

  }
}


int TotalEntriesM5K = PadDisplayADC2->GetEntries();
double ChargeAveM5K = TotalCharge2/TotalEntriesM5K;

#ifdef COSMIC
      for (Int_t it_j = 0; it_j < 48; ++it_j) {
#else
      for (Int_t it_i = 0; it_i < 36; ++it_i) {
#endif

#ifdef COSMIC
        for (Int_t it_i = 0; it_i < 36; ++it_i) {
#else
        for (Int_t it_j = 0; it_j < 48; ++it_j) {
#endif

  if(!PadDisplayADC2->GetBinContent(it_i+1,it_j+1)) continue;   
  PadDisplayADC2->SetBinContent(it_i+1,it_j+1,PadDisplayADC2->GetBinContent(it_i+1,it_j+1)/ChargeAveM5K);

  }
}

PadDisplayADC1->Divide(PadDisplayCnt1);
PadDisplayADC2->Divide(PadDisplayCnt2);


TFile* out_file = new TFile((prefix + "inter_calib_CosmicTrack.root").Data(), "RECREATE");

  TCanvas *c1 = new TCanvas("c1","c1", 0, 0, 1000, 800);
  TCanvas *c2 = new TCanvas("c2","c2", 0, 0, 1000, 800);
  TCanvas *c3 = new TCanvas("c3","c3", 0, 0, 1000, 800);
  TCanvas *c4 = new TCanvas("c4","c4", 0, 0, 1000, 800);
  TCanvas *c5 = new TCanvas("c5","c5", 0, 0, 1000, 800);
  TCanvas *c6 = new TCanvas("c6","c6", 0, 0, 1000, 800);
  TCanvas *c7 = new TCanvas("c7","c7", 0, 0, 1000, 800);
  TCanvas *c8 = new TCanvas("c8","c8", 0, 0, 1000, 800);
  TCanvas *c9 = new TCanvas("c9","c9", 0, 0, 1000, 800);

//  gStyle->SetOptStat("RMne");
//  gStyle->SetPalette(55);
  //inter_cal->Divide(3,3);

  //int ratio = 1;

  

  // c5->cd();
  // NoApl_In_cal->Divide(PadDisplayHIT2);
  // NoApl_In_cal->Draw("LEGO2Z");
  // NoApl_In_cal->Write();

  // // c3->cd();
  // // Apl_In_cal->Divide(PadDisplayHIT2);
  // // Apl_In_cal->Scale(ratio);
  // // Apl_In_cal->Draw("LEGO2Z");
  // // Apl_In_cal->Write();

  // c4->cd();
  // NoApl_In_cal->GetZaxis()->SetRangeUser(0,1);
  // NoApl_In_cal->Draw("COLZ");

  // // c5->cd();
  // // In_cal->Divide(PadDisplayHIT1);
  // // In_cal->Scale(ratio);
  // // In_cal->Draw("LEGO2Z");
  // // In_cal->Write();

  // c6->cd();
  // In_cal->Scale(ratio);
  // r_n->Draw("COL");
  // r_n->Write();
  
  c7->cd();
  PadDisplayADC->Divide(PadDisplayCnt);
  PadDisplayADC->Draw("colz");
  PadDisplayADC->Write();

  c8->cd();
  PadDisplayADC1->GetZaxis()->SetRangeUser(0,2);
  PadDisplayADC1->Draw("colz");
  PadDisplayADC1->Write();

  c9->cd();
  PadDisplayADC2->GetZaxis()->SetRangeUser(0,2); 
  PadDisplayADC2->Divide(PadDisplayADC1);
  PadDisplayADC2->Draw("colz");
  PadDisplayADC2->Write();

  c1->cd();
  PadDisplayADC->Draw("LEGO2Z");

  c2->cd();
  PadDisplayADC1->Draw("LEGO2Z");

  c3->cd();
  PadDisplayADC2->Draw("LEGO2Z");

c9->Update();
c8->Update();
c7->Update();
c6->Update();
c5->Update();
c4->Update();
c3->Update();
c2->Update();
c1->Update();

  out_file->Close();

  cout << "Total events number     : " << total_events << endl;
  cout << "total_NOiron_events     : " << total_NOiron_events << endl;
  cout << "Track events number     : " << sel_events << endl;
    
return;
}
