#!bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DIR="$DIR/../app/"

if [ ! -d "$DIR" ]; then
  mkdir "${DIR}"
fi

name="${1%.*}"
echo "******* Compiling ${name}.C to ${name}.exe *******"
make SRC="../mydict.C ${name}.C" EXE="${DIR}${name}.exe"