#!bin/sh

rm -r temp
mkdir temp

i=0
while IFS=$'\n' read file || [[ -n "$file" ]]; do
  echo "#PBS -q medium" >> "temp/job${i}.sub"
  echo "#PBS -N job_iron_${i}" >> "temp/job${i}.sub"
  echo "#PBS -l nodes=1" >> "temp/job${i}.sub"
  echo "" >> "temp/job${i}.sub"
  echo "cd /software/neutrinos/TPC_TestBeam/app" >> "temp/job${i}.sub"
  echo "${file}" >> "temp/job${i}.sub"
  echo "${i}   ${file}"
  qsub temp/job${i}.sub -o /software/neutrinos/TPC_TestBeam/jobs/jobsOUT/out/output_${i}.txt -e /software/neutrinos/TPC_TestBeam/jobs/jobsOUT/err/outerr_{i}.txt
  i=$((($i+1)))
done < "$1"
