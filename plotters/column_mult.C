#include "../macros/ilc/common_header.h"

void column_mult(){

  Int_t T2KstyleIndex = 3;
  // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex-1;

  TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
  gROOT->SetStyle(t2kstyle->GetName());

  TString prefix = "/eos/user/s/ssuvorov/FIGURE/TPC_beamtest/v14/Pion5/";
  TFile* file     = new TFile((prefix + "/output_adv.root").Data());

  TH1F* col_mult[36];
  TGraph* gr = new TGraph();

  for (int i = 0; i < 36; ++i) {
    col_mult[i] = (TH1F*)file->Get(Form("PadsPerCluster_%i", i));
    if (!col_mult[i])
      continue;
    if (!col_mult[i]->Integral())
      continue;

    gr->SetPoint(gr->GetN(), i, col_mult[i]->GetMean());
  }

  TCanvas *c1     = new TCanvas("c1","evadc1",0, 0, 800,630);
  //gr->Draw("ap");
  TH1F* mult = (TH1F*)file->Get("PadsPerCluster");
  mult->Draw();
  c1->Print((prefix + "col_mult.pdf").Data());
}