#include "TGraph.h"
#include "TCanvas.h"
#include "TAxis.h"

void source_gain() {
  TGraph* gr = new TGraph();

  // total number of ADCs
  float Nadc        = 4096.;
  // charge in fC that corresponds to all ADC range
  float Ncharge     = 120.;
  float e_charge    = 1.60218E-4;
  // number of electrons per 5.59 kEv event in Argon
  float Nelectrons  = 230.;

  float coef        = Nelectrons * Nadc * e_charge / Ncharge;

  float x[6] = {330.,   340.,   350.,   360.,   370.,   380.};
  float y[6] = {445.6,  711.8,  925.8,  1462.,  2021.,  3284.};
  float g[6];

  for (Int_t i = 0; i < 6; ++i) {
    g[i] = y[i] / coef;
    gr->SetPoint(gr->GetN(), x[i], g[i]);
  }

  gr->GetXaxis()->SetTitle("MM voltage, V");
  gr->GetYaxis()->SetTitle("Gain");

  gr->SetMarkerStyle(20);
  gr->SetMarkerSize(0.5);

  TCanvas *c1 = new TCanvas("c1","evadc1",0, 0, 400,300);
  gr->Draw("ap");
  c1->Print("/eos/user/s/ssuvorov/FIGURE/TPC_beamtest/source/gain.pdf");
}