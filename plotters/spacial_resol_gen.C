#include "../macros/ilc/common_header.h"
#include "TMultiGraph.h"

TH1F* Scale(TH1F* h, float scale) {
  TH1F* out = (TH1F*)h->Clone("scaled");
  for (int ibin = 1; ibin <= h->GetXaxis()->GetNbins(); ++ibin) {
    out->SetBinContent(ibin, h->GetBinContent(ibin) * scale);
    out->SetMinimum(0.);
    out->SetMaximum(600.);
  }
  return out;
}

void PlotEach(TH1F* b, TH1F* a, TString prefix, TCanvas* c = NULL, int i = -1) {
  TCanvas *c_temp     = new TCanvas("c_temp","evadc1",0, 0, 400,300);
  b->GetXaxis()->SetTitle("Column");
  b->GetYaxis()->SetTitle("Resolution, #mu m");
  gStyle->SetOptStat(0);
  b->Draw();
  a->SetLineColor(kRed);
  a->Draw("same");
  c_temp->Print((prefix + "/resol.pdf").Data());

  if (c && i != -1) {
    c->cd(i);
    TH1F* b_s = Scale(b, 10000);
    TH1F* a_s = Scale(a, 10000);

    b_s->Draw();
    a_s->Draw("same");
  }
}

float GetAverage(TH1F* h, float& RMS) {
  int N = 0;
  float av = 0;
  TCanvas *c3     = new TCanvas("c2","evadc1",0, 0, 800,630);
  TH1F* resol = new TH1F("res", "", 50, 200, 700);
  for (int i = 2; i <= h->GetXaxis()->GetNbins() - 1; ++i) {
    ++N;
    av += h->GetBinContent(i);
    resol->Fill(10000*h->GetBinContent(i));
  }

  av /= N;
  resol->Fit("gaus");
  //resol->Fit("gaus");
  //resol->Fit("gaus");

  RMS = 0.;
  for (int i = 2; i <= h->GetXaxis()->GetNbins() - 1; ++i) {
    RMS += (h->GetBinContent(i) - av) * (h->GetBinContent(i) - av);
  }
  RMS /= N;
  RMS = sqrt(RMS);

  //RMS = resol->GetFunction("gaus")->GetParError(1) / 10000.;
  //cout << resol->GetFunction("gaus")->GetChisquare() << "/" << resol->GetFunction("gaus")->GetNDF() << endl;
  //resol->Draw();
  //c3->Print("/eos/user/s/ssuvorov/FIGURE/TPC_beamtest/v14/SR/test.pdf");
  //int a;
  //cin >> a;
  delete resol;
  return av;
}

void spacial_resol_gen(){

  Int_t T2KstyleIndex = 3;
  // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex;

  TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
  gROOT->SetStyle(t2kstyle->GetName());
  gROOT->ForceStyle();

  TCanvas *c1     = new TCanvas("c1","evadc1",0, 0, 800,630);
  //c1->SetGrid(1);
  TCanvas *c2     = new TCanvas("c2","evadc1",0, 0, 800,630);
  c2->Divide(1, 2);

  TCanvas *c_tot  = new TCanvas("c_tot","evadc1",0, 0, 1200,900);
  c_tot->Divide(3, 3);

  TCanvas *c_tot_2  = new TCanvas("c_tot_2","evadc1",0, 0, 1200,300);
  c_tot_2->Divide(3, 1);

  TString prefix      = "/eos/user/s/ssuvorov/FIGURE/TPC_beamtest/v14/";
  TString file_name   = "/output_prf_uni.root";

  TString file_Prot1   = prefix + "Pr1" + file_name;
  TString file_Elec1   = prefix + "Ele1" + file_name;
  TString file_Pion1   = prefix + "Pion1" + file_name;

  TString file_Prot2   = prefix + "Pr2" + file_name;
  TString file_Elec2   = prefix + "Ele2" + file_name;
  TString file_Pion2   = prefix + "Pion2" + file_name;

  TString file_Prot3   = prefix + "Pr3" + file_name;
  TString file_Elec3   = prefix + "Ele3" + file_name;
  TString file_Pion3   = prefix + "Pion3" + file_name;

  TString file_Prot4   = prefix + "Pr4" + file_name;
  TString file_Elec4   = prefix + "Ele4" + file_name;
  TString file_Pion4   = prefix + "Pion4" + file_name;

  TFile* f_Prot1      = new TFile(file_Prot1.Data(), "READ");
  TFile* f_Elec1      = new TFile(file_Elec1.Data(), "READ");
  TFile* f_Pion1      = new TFile(file_Pion1.Data(), "READ");
  TFile* f_Prot2      = new TFile(file_Prot2.Data(), "READ");
  TFile* f_Elec2      = new TFile(file_Elec2.Data(), "READ");
  TFile* f_Pion2      = new TFile(file_Pion2.Data(), "READ");
  TFile* f_Prot3      = new TFile(file_Prot3.Data(), "READ");
  TFile* f_Elec3      = new TFile(file_Elec3.Data(), "READ");
  TFile* f_Pion3      = new TFile(file_Pion3.Data(), "READ");
  /*TFile* f_Prot4      = new TFile(file_Prot4.Data(), "READ");
  TFile* f_Elec4      = new TFile(file_Elec4.Data(), "READ");
  TFile* f_Pion4      = new TFile(file_Pion4.Data(), "READ");*/

  TH1F* h_Prot1_b        = (TH1F*)f_Prot1->Get("resol_final_0");
  TH1F* h_Elec1_b        = (TH1F*)f_Elec1->Get("resol_final_0");
  TH1F* h_Pion1_b        = (TH1F*)f_Pion1->Get("resol_final_0");

  TH1F* h_Prot2_b        = (TH1F*)f_Prot2->Get("resol_final_0");
  TH1F* h_Elec2_b        = (TH1F*)f_Elec2->Get("resol_final_0");
  TH1F* h_Pion2_b        = (TH1F*)f_Pion2->Get("resol_final_0");

  TH1F* h_Prot3_b        = (TH1F*)f_Prot3->Get("resol_final_0");
  TH1F* h_Elec3_b        = (TH1F*)f_Elec3->Get("resol_final_0");
  TH1F* h_Pion3_b        = (TH1F*)f_Pion3->Get("resol_final_0");

  /*TH1F* h_Prot4_b        = (TH1F*)f_Prot4->Get("resol_final_0");
  TH1F* h_Elec4_b        = (TH1F*)f_Elec4->Get("resol_final_0");
  TH1F* h_Pion4_b        = (TH1F*)f_Pion4->Get("resol_final_0");*/

  TH1F* h_Prot1_a        = (TH1F*)f_Prot1->Get("resol_final_4");
  TH1F* h_Elec1_a        = (TH1F*)f_Elec1->Get("resol_final_4");
  TH1F* h_Pion1_a        = (TH1F*)f_Pion1->Get("resol_final_4");

  TH1F* h_Prot2_a        = (TH1F*)f_Prot2->Get("resol_final_4");
  TH1F* h_Elec2_a        = (TH1F*)f_Elec2->Get("resol_final_4");
  TH1F* h_Pion2_a        = (TH1F*)f_Pion2->Get("resol_final_4");

  TH1F* h_Prot3_a        = (TH1F*)f_Prot3->Get("resol_final_4");
  TH1F* h_Elec3_a        = (TH1F*)f_Elec3->Get("resol_final_4");
  TH1F* h_Pion3_a        = (TH1F*)f_Pion3->Get("resol_final_4");

  /*TH1F* h_Prot4_a        = (TH1F*)f_Prot4->Get("resol_final_4");
  TH1F* h_Elec4_a        = (TH1F*)f_Elec4->Get("resol_final_4");
  TH1F* h_Pion4_a        = (TH1F*)f_Pion4->Get("resol_final_4");*/

  // Plot before and after for each sample
  PlotEach(h_Prot1_b, h_Prot1_a, prefix + "Pr1", c_tot, 1);
  PlotEach(h_Prot2_b, h_Prot2_a, prefix + "Pr2", c_tot, 2);
  PlotEach(h_Prot3_b, h_Prot3_a, prefix + "Pr3", c_tot, 3);
  //PlotEach(h_Prot4_b, h_Prot4_a, prefix + "Pr4", c_tot, -1);

  PlotEach(h_Elec1_b, h_Elec1_a, prefix + "Ele1", c_tot, 4);
  PlotEach(h_Elec2_b, h_Elec2_a, prefix + "Ele2", c_tot, 5);
  PlotEach(h_Elec3_b, h_Elec3_a, prefix + "Ele3", c_tot, 6);
  //PlotEach(h_Elec4_b, h_Elec4_a, prefix + "Ele4", c_tot, -1);

  PlotEach(h_Pion1_b, h_Pion1_a, prefix + "Pion1", c_tot, 7);
  PlotEach(h_Pion2_b, h_Pion2_a, prefix + "Pion2", c_tot, 8);
  PlotEach(h_Pion3_b, h_Pion3_a, prefix + "Pion3", c_tot, 9);
  //PlotEach(h_Pion4_b, h_Pion4_a, prefix + "Pion4", c_tot, -1);

  c_tot->Print((prefix + "/SR/b_a.pdf"));

  c_tot_2->cd();
  PlotEach(h_Prot3_b, h_Prot3_a, prefix + "Pr3", c_tot_2, 1);
  PlotEach(h_Elec3_b, h_Elec3_a, prefix + "Ele3", c_tot_2, 2);
  PlotEach(h_Pion3_b, h_Pion3_a, prefix + "Pion3", c_tot_2, 3);
  c_tot_2->Print((prefix + "/SR/b_a_10cm.pdf"));


  // for all particles and distances plot the average before and after
  float prot_80_RMS, prot_30_RMS, prot_10_RMS;
  float elec_80_RMS, elec_30_RMS, elec_10_RMS;
  float pion_80_RMS, pion_30_RMS, pion_10_RMS;
  float prot_80 = 10000. * GetAverage(h_Prot1_a, prot_80_RMS);
  float prot_30 = 10000. * GetAverage(h_Prot2_a, prot_30_RMS);
  float prot_10 = 10000. * GetAverage(h_Prot3_a, prot_10_RMS);

  float elec_80 = 10000. * GetAverage(h_Elec1_a, elec_80_RMS);
  float elec_30 = 10000. * GetAverage(h_Elec2_a, elec_30_RMS);
  float elec_10 = 10000. * GetAverage(h_Elec3_a, elec_10_RMS);

  float pion_80 = 10000. * GetAverage(h_Pion1_a, pion_80_RMS);
  float pion_30 = 10000. * GetAverage(h_Pion2_a, pion_30_RMS);
  float pion_10 = 10000. * GetAverage(h_Pion3_a, pion_10_RMS);

  cout << prot_80_RMS << "  " << prot_30_RMS << "   " << prot_10_RMS << endl;

  TGraphErrors* prot = new TGraphErrors();
  TGraphErrors* elec = new TGraphErrors();
  TGraphErrors* pion = new TGraphErrors();

  //Float_t marker_size = 1.5;

  prot->SetMarkerStyle(20);
  prot->SetMarkerColor(kBlack);
  //prot->SetMarkerSize(marker_size);
  elec->SetMarkerStyle(20);
  elec->SetMarkerColor(kBlue);
  //elec->SetMarkerSize(marker_size);
  pion->SetMarkerStyle(20);
  pion->SetMarkerColor(kRed);
  //pion->SetMarkerSize(marker_size);

  prot->SetPoint(0, 80, prot_80);
  prot->SetPointError(0, 0., 10000 * prot_80_RMS);
  prot->SetPoint(1, 30, prot_30);
  prot->SetPointError(1, 0., 10000 * prot_30_RMS);
  prot->SetPoint(2, 10, prot_10);
  prot->SetPointError(2, 0., 10000 * prot_10_RMS);

  elec->SetPoint(0, 80, elec_80);
  elec->SetPointError(0, 0., 10000 * elec_80_RMS);
  elec->SetPoint(1, 30, elec_30);
  elec->SetPointError(1, 0., 10000 * elec_30_RMS);
  elec->SetPoint(2, 10, elec_10);
  elec->SetPointError(2, 0., 10000 * elec_10_RMS);

  pion->SetPoint(0, 80, pion_80);
  pion->SetPointError(0, 0., 10000 * pion_80_RMS);
  pion->SetPoint(1, 30, pion_30);
  pion->SetPointError(1, 0., 10000 * pion_30_RMS);
  pion->SetPoint(2, 10, pion_10);
  pion->SetPointError(2, 0., 10000 * pion_10_RMS);

  TMultiGraph* mgr = new TMultiGraph();
  mgr->Add(prot, "p");
  mgr->Add(elec, "p");
  mgr->Add(pion, "p");

  TLegend* l1=new TLegend(gStyle->GetPadLeftMargin()+0.1,
                          1-gStyle->GetPadTopMargin() - 0.35,
                          gStyle->GetPadLeftMargin() + 0.4,
                          1-gStyle->GetPadTopMargin() - 0.1);
  l1->AddEntry(prot, "Protons", "p");
  l1->AddEntry(elec, "Electrons", "p");
  l1->AddEntry(pion, "Pions", "p");

  mgr->GetXaxis()->SetTitle("Drift distance [cm]");
  mgr->GetYaxis()->SetTitle("Resolution [#mum]");

  //c1->SetGrid(1);

  c1->cd();
  mgr->Draw("a");
  mgr->GetXaxis()->SetLimits(0., 90.);
  mgr->GetYaxis()->SetRangeUser(200., 600.);
  gPad->Modified();
  l1->Draw();
  c1->Print((prefix + "SR/resol_drift.pdf"));

  // Draw the resolution vs. drift for both CoC and PRF
  prot->SetMarkerColor(kRed);
  elec->SetMarkerColor(kRed);
  pion->SetMarkerColor(kRed);

  float prot_80_RMS_b, prot_30_RMS_b, prot_10_RMS_b;
  float elec_80_RMS_b, elec_30_RMS_b, elec_10_RMS_b;
  float pion_80_RMS_b, pion_30_RMS_b, pion_10_RMS_b;
  float prot_80_b = 10000. * GetAverage(h_Prot1_b, prot_80_RMS_b);
  float prot_30_b = 10000. * GetAverage(h_Prot2_b, prot_30_RMS_b);
  float prot_10_b = 10000. * GetAverage(h_Prot3_b, prot_10_RMS_b);
  float elec_80_b = 10000. * GetAverage(h_Elec1_b, elec_80_RMS_b);
  float elec_30_b = 10000. * GetAverage(h_Elec2_b, elec_30_RMS_b);
  float elec_10_b = 10000. * GetAverage(h_Elec3_b, elec_10_RMS_b);
  float pion_80_b = 10000. * GetAverage(h_Pion1_b, pion_80_RMS_b);
  float pion_30_b = 10000. * GetAverage(h_Pion2_b, pion_30_RMS_b);
  float pion_10_b = 10000. * GetAverage(h_Pion3_b, pion_10_RMS_b);


  TGraphErrors* prot_b = new TGraphErrors();
  TGraphErrors* elec_b = new TGraphErrors();
  TGraphErrors* pion_b = new TGraphErrors();

  prot_b->SetMarkerStyle(20);
  prot_b->SetMarkerColor(kBlue);
  //prot_b->SetMarkerSize(marker_size);
  elec_b->SetMarkerStyle(21);
  elec_b->SetMarkerColor(kBlue);
  //elec_b->SetMarkerSize(marker_size);
  pion_b->SetMarkerStyle(22);
  pion_b->SetMarkerColor(kBlue);
  //pion_b->SetMarkerSize(marker_size);

  prot_b->SetPoint(0, 80, prot_80_b);
  prot_b->SetPointError(0, 0., 10000 * prot_80_RMS_b);
  prot_b->SetPoint(1, 30, prot_30_b);
  prot_b->SetPointError(1, 0., 10000 * prot_30_RMS_b);
  prot_b->SetPoint(2, 10, prot_10_b);
  prot_b->SetPointError(2, 0., 10000 * prot_10_RMS_b);
  elec_b->SetPoint(0, 80, elec_80_b);
  elec_b->SetPointError(0, 0., 10000 * elec_80_RMS_b);
  elec_b->SetPoint(1, 30, elec_30_b);
  elec_b->SetPointError(1, 0., 10000 * elec_30_RMS_b);
  elec_b->SetPoint(2, 10, elec_10_b);
  elec_b->SetPointError(2, 0., 10000 * elec_10_RMS_b);
  pion_b->SetPoint(0, 80, pion_80_b);
  pion_b->SetPointError(0, 0., 10000 * pion_80_RMS_b);
  pion_b->SetPoint(1, 30, pion_30_b);
  pion_b->SetPointError(1, 0., 10000 * pion_30_RMS_b);
  pion_b->SetPoint(2, 10, pion_10_b);
  pion_b->SetPointError(2, 0., 10000 * pion_10_RMS_b);

  //mgr->Add(prot_b, "p");
  //mgr->Add(elec_b, "p");
  TMultiGraph* mgr3 = new TMultiGraph();
  mgr3->Add(pion_b, "p");

  l1->SetHeader("PRF method", "C");

  TLegend* l2=new TLegend(gStyle->GetPadLeftMargin() + 0.3,
                          gStyle->GetPadBottomMargin()+0.05,
                          gStyle->GetPadLeftMargin() + 0.6,
                          gStyle->GetPadBottomMargin()+0.3);
  l2->AddEntry(prot_b, "Protons", "p");
  l2->AddEntry(elec_b, "Electrons", "p");
  l2->AddEntry(pion_b, "Pions", "p");
  l2->SetHeader("CoC method", "C");

  c1->cd();
  mgr3->Draw("a");
  mgr3->GetXaxis()->SetLimits(0., 90.);
  mgr3->GetYaxis()->SetRangeUser(200., 700.);
  mgr3->GetXaxis()->SetTitle("Drift distance [cm]");
  mgr3->GetYaxis()->SetTitle("Resolution [#mum]");

  //gPad->Modified();
  //l1->Draw();
  //l2->Draw();
  c1->Print((prefix + "SR/resol_drift_2.pdf"));

  // end of plotting the resolution


  const int N_col = 9;
  int col_id[N_col] = {2, 7, 9, 13, 17, 20, 24, 30, 35};

  TGraph* ele_column[N_col];
  TMultiGraph* mgr2 = new TMultiGraph();
  TH1F* h;

  /*for (Int_t i = 0; i < N_col; ++i) {
    ele_column[i] = new TGraph();
    ele_column[i]->SetPoint(0, 80., 10000. * h_Elec1_a->GetBinContent(col_id[i]));
    ele_column[i]->SetPoint(1, 30., 10000. * h_Elec2_a->GetBinContent(col_id[i]));
    ele_column[i]->SetPoint(2, 10., 10000. * h_Elec3_a->GetBinContent(col_id[i]));

    ele_column[i]->SetMarkerColor(i+1);
    ele_column[i]->SetMarkerStyle(21);
    ele_column[i]->SetMarkerSize(0.6);

    c1->cd();
    gStyle->SetOptFit(1);
    gStyle->SetOptStat(1);
    h = (TH1F*)f_Elec3->Get(Form("col_by_col/resol_histo_0_%i", col_id[i] - 1));
    h->Draw();
    c1->Print((prefix + Form("SR/iter_0_col_%i.pdf", col_id[i] - 1)));

    h = (TH1F*)f_Elec3->Get(Form("col_by_col/resol_histo_4_%i", col_id[i] - 1));
    h->Draw();
    c1->Print((prefix + Form("SR/iter_4_col_%i.pdf", col_id[i] - 1)));

    mgr2->Add(ele_column[i], "p");
  }*/

  c1->cd();
  mgr2->Draw("a");
  mgr2->GetXaxis()->SetLimits(0., 90.);
  mgr2->GetYaxis()->SetRangeUser(200., 550.);
  gPad->Modified();
  c1->Print((prefix + "SR/columns.pdf"));

  exit(1);
}
