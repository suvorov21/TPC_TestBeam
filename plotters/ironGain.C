#include "../macros/ilc/common_header.h"
#include "../macros/ilc/ini.C"
#include "TMultiGraph.h"
#include "TAxis.h"

void ironGain() {

  Int_t T2KstyleIndex = 1;
  // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex-1;

  TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
  gROOT->SetStyle(t2kstyle->GetName());
  
  TCanvas *c1 = new TCanvas("c1","c1",0, 0, 1200,900);
  TCanvas *c_tot  = new TCanvas("c_tot","c_tot",0, 0, 1200,900);
  c_tot->Divide(3, 3);

  TF1* fit        = NULL;
  //TF1* f2peak = new TF1("f2peak", "[0] * TMath::Gaus(x, [1], [2]) + [3] * TMath::Gaus(x, [4], [5])", 0,15000);
  TF1* f2peak = new TF1("f2peak", "gaus", 600,1200);

  f2peak->SetParName(0, "Const1");
  f2peak->SetParName(1, "Mean1");
  f2peak->SetParName(2, "Sigma1");
  f2peak->SetParName(3, "Const2");
  f2peak->SetParName(4, "Mean2");
  f2peak->SetParName(5, "Sigma2");

  float Nadc        = 4096.;
  // charge in fC that corresponds to all ADC range
  float Ncharge     = 120.;
  float e_charge    = 1.60218E-4;
  // number of electrons per 5.59 kEv event in Argon
  float Nelectrons  = 230.;

  float coef        = Nelectrons * Nadc * e_charge / Ncharge;

  //f2peak->SetParameters(100, 4000, 500, 800, 10000, 1000);
  f2peak->SetParameters(1500,900,200);

  float toffset = 24+6-0.5;
  float tFirst[9] = {19.24,24+10+22./60,24+13+36./60,24+18+24./60, 48+9./60,48+12+30./60, 48+17+20./60,48+23+20./60,72+8};
  float tLast[9] = {24+1+40./60,24+13+36./60,24+18+24./60, 48+9./60,48+12+30./60, 48+17+20./60,48+23+20./60,72+8,72+16};

  //ANOTHER GAP HAPPENS IN RUNS 368 to 374!!!! Manually solved down.
  int tlocal = 0;

  /// THERE WAS A GAP BETWEEN RUNS 338 AND 339, SOME CODE TO FIX IT
  float tSpecialFirst = 24+6;
  float tSpecialLast = 24+9+22./60;
  ///
  
  float numfiles[9] = {8,3,4,4,3,3,4,6,4};
  int numevts[9] = {202,89,103,100,93,80,100,158,105};

  for(int j = 0; j<9; j++){
    tFirst[j] = tFirst[j]-toffset;
    tLast[j] = tLast[j]-toffset;
  }

  tSpecialLast-= toffset;
  tSpecialFirst-= toffset;

  TGraphErrors *gr_b = new TGraphErrors();
  TGraphErrors *gr_a = new TGraphErrors();

  gr_b->SetFillColor(0);
  gr_a->SetFillColor(0);

  gr_b->SetMarkerStyle(20);
  gr_a->SetMarkerStyle(20);

  gr_b->SetMarkerSize(1);
  gr_a->SetMarkerSize(1);

  TMultiGraph* mgr = new TMultiGraph();

  cout << "coef: " << coef << endl;

  for(int j = 0; j<9; j++){
    cout << "tLast: " << tLast[j] << endl;
    cout << "1_per var: " << (numevts[j]-25*numfiles[j])/numevts[j] << endl;//(tLast[j]-tFirst[j])*(numevts[j]-25*numfiles[j])/numevts[j] << endl;
    tLast[j] = tLast[j]-(tLast[j]-tFirst[j])*(numevts[j]-25*numfiles[j])/numevts[j];
    cout << "tLast: " << tLast[j] << endl;
  }

  tSpecialLast = tSpecialLast-(tSpecialLast-tSpecialFirst)*(87-25*3)/87;

  TString prefix      = "/Users/cjesus/Desktop/FIGURES/TestBeam_DBSCAN/FIGURES/iron/";
  TString file_name   = "/output_iron.root";

  TString file_Pr1    = prefix + "Pr1" + file_name;
  TString file_Ele1   = prefix + "Ele1" + file_name;
  TString file_Pion1  = prefix + "Pion1" + file_name;

  TString file_Pr2    = prefix + "Pr2" + file_name;
  TString file_Ele2   = prefix + "Ele2" + file_name;
  TString file_Pion2  = prefix + "Pion2" + file_name;

  TString file_Pr3    = prefix + "Pr3" + file_name;
  TString file_Ele3   = prefix + "Ele3" + file_name;
  TString file_Pion3  = prefix + "Pion3" + file_name;

  TFile* f_Pr1        = new TFile(file_Pr1.Data(), "READ");
  TFile* f_Ele1       = new TFile(file_Ele1.Data(), "READ");
  TFile* f_Pion1      = new TFile(file_Pion1.Data(), "READ");
  TFile* f_Pr2        = new TFile(file_Pr2.Data(), "READ");
  TFile* f_Ele2       = new TFile(file_Ele2.Data(), "READ");
  TFile* f_Pion2      = new TFile(file_Pion2.Data(), "READ");
  TFile* f_Pr3        = new TFile(file_Pr3.Data(), "READ");
  TFile* f_Ele3       = new TFile(file_Ele3.Data(), "READ");
  TFile* f_Pion3      = new TFile(file_Pion3.Data(), "READ");


  for (int a=0; a<=12; a++){

  char name_it[256];
  sprintf(name_it,"_%d", a);
  TString name_ext = name_it; 

  TH1F* h_Pr1          = (TH1F*)f_Pr1->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Ele1         = (TH1F*)f_Ele1->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Pion1        = (TH1F*)f_Pion1->Get(("Spectrum"+name_ext).Data());

  TH1F* h_Pr2          = (TH1F*)f_Pr2->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Ele2         = (TH1F*)f_Ele2->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Pion2        = (TH1F*)f_Pion2->Get(("Spectrum"+name_ext).Data());

  TH1F* h_Pr3          = (TH1F*)f_Pr3->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Ele3         = (TH1F*)f_Ele3->Get(("Spectrum"+name_ext).Data());
  TH1F* h_Pion3        = (TH1F*)f_Pion3->Get(("Spectrum"+name_ext).Data());

  float mean = 0;
  float meanErr = 0;

  c_tot->cd(1);

  cout << endl << "-------------------" << endl;
  cout << "a:    " << a << endl;
  cout << endl << "-------------------" << endl;

  if(numfiles[2]>a){
    h_Pr1->Fit("f2peak","RQ");
    fit = h_Pr1->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Pr1" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_b->SetPoint(gr_b->GetN(), tFirst[2]+(tLast[2]-tFirst[2])*(a+0.5)*(25*numfiles[2]/numevts[2])/numfiles[2], mean/coef);
    gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
  }

  c_tot->cd(2);

  if(numfiles[3]>a){
    h_Pr2->Fit("f2peak","RQ");
    fit = h_Pr2->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Pr2" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_b->SetPoint(gr_b->GetN(), tFirst[3]+(tLast[3]-tFirst[3])*(a+0.5)*(25*numfiles[3]/numevts[3])/numfiles[3], mean/coef);
    gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
  }

  c_tot->cd(3);

  if(numfiles[6]>a){
    h_Pr3->Fit("f2peak","RQ");
    fit = h_Pr3->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Pr3" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_a->SetPoint(gr_a->GetN(), tFirst[6]+(tLast[6]-tFirst[6])*(a+0.5)*(25*numfiles[6]/numevts[6])/numfiles[6], mean/coef);
    gr_a->SetPointError(gr_a->GetN()-1, 0, meanErr/coef);
  }
  c_tot->cd(4);

  if(numfiles[1]>a){
    cout << "a is: " << a << endl;
    h_Ele1->Fit("f2peak","RQ");
    fit = h_Ele1->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Ele1" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_b->SetPoint(gr_b->GetN(), tFirst[1]+(tLast[1]-tFirst[1])*(a+0.5)*(25*numfiles[1]/numevts[1])/numfiles[1], mean/coef);
    gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
  }
  c_tot->cd(5);

  if(numfiles[5]>a){
    cout << "a is: " << a << endl;
    h_Ele2->Fit("f2peak","RQ");
    fit = h_Ele2->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Ele2" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_a->SetPoint(gr_a->GetN(), tFirst[5]+(tLast[5]-tFirst[5])*(a+0.5)*(25*numfiles[5]/numevts[5])/numfiles[5], mean/coef);
    gr_a->SetPointError(gr_a->GetN()-1, 0, meanErr/coef);
  }
  c_tot->cd(6);

  if(numfiles[8]>a){
    h_Ele3->Fit("f2peak","RQ");
    fit = h_Ele3->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Ele3" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_a->SetPoint(gr_a->GetN(), tFirst[8]+(tLast[8]-tFirst[8])*(a+0.5)*(25*numfiles[8]/numevts[8])/numfiles[8], mean/coef);
    gr_a->SetPointError(gr_a->GetN()-1, 0, meanErr/coef);
  }

  c_tot->cd(7);
  if(numfiles[0]+3>a){
    if(a<=7){
      h_Pion1->Fit("f2peak","RQ");
      fit = h_Pion1->GetFunction("f2peak");
      mean  = fit->GetParameter(1);
      meanErr = fit->GetParError(1);
      cout << "Pion1" << endl;
      cout << "mean: " << mean/coef << endl;
      gr_b->SetPoint(gr_b->GetN(), tFirst[0]+(tLast[0]-tFirst[0])*(a+0.5)*(25*numfiles[0]/numevts[0])/numfiles[0], mean/coef);
      gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
    }
    if(a>8){
      cout << "AAAAAAAAAAAHHHHHH" << endl;
      h_Pion1->Fit("f2peak","RQ");
      fit = h_Pion1->GetFunction("f2peak");
      mean  = fit->GetParameter(1);
      meanErr = fit->GetParError(1);
      cout << "Pion1" << endl;
      cout << "mean: " << mean/coef << endl;
      gr_b->SetPoint(gr_b->GetN(), tSpecialFirst+(tSpecialLast-tSpecialFirst)*(a-7+0.5)*(25*3./87)/3, mean/coef);
      gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
    }
  }


  c_tot->cd(8);

  if(numfiles[4]>a){
    if(a==0) tlocal = 49+20./60-toffset;
    if(a==1) tlocal = 48+4+40./60  -toffset;
    if(a==2) tlocal = 48+8 -toffset;
    if(a==3) tlocal = 48+9+30./60 -toffset;
    h_Pion2->Fit("f2peak","RQ");
    fit = h_Pion2->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Pion2" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_b->SetPoint(gr_b->GetN(), tlocal, mean/coef);
    gr_b->SetPointError(gr_b->GetN()-1, 0, meanErr/coef);
  }

  c_tot->cd(9);

  if(numfiles[7]>a){
    h_Pion3->Fit("f2peak","RQ");
    fit = h_Pion3->GetFunction("f2peak");
    mean  = fit->GetParameter(1);
    meanErr = fit->GetParError(1);
    cout << "Pion3" << endl;
    cout << "mean: " << mean/coef << endl;
    gr_a->SetPoint(gr_a->GetN(), tFirst[7]+(tLast[7]-tFirst[7])*(a+0.5)*(25*numfiles[7]/numevts[7])/numfiles[7], mean/coef);
    gr_a->SetPointError(gr_a->GetN()-1, 0, meanErr/coef);
  }

  c_tot->Print((prefix+"ironSpectrum.pdf").Data());

  }

  c1->cd();
  mgr->Add(gr_b, "p");
  mgr->Add(gr_a, "p");

  mgr->Draw("a");
  gPad->SetLeftMargin(0.15);
  mgr->GetYaxis()->SetTitleOffset(1.4);
  mgr->GetYaxis()->SetTitle("Gain");
  mgr->GetXaxis()->SetTitle("Time [hours]");
  mgr->GetXaxis()->SetLimits(0,59.5);


  // TLegend* l=new TLegend(0.4,0.4,0.6,0.6);
  // l->AddEntry(gr_prot, "Protons");
  // l->AddEntry(gr_elec, "Electrons");
  // l->AddEntry(gr_pion, "Pions");
  // l->SetFillStyle(0);
//  mgr->Draw("a");
  // l->Draw();
  c1->Print((prefix+"gain.pdf").Data());

}
